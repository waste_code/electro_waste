EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:ESP8266
LIBS:switches
LIBS:canon_trigger-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L 4N25 U2
U 1 1 5A3AE162
P 9750 4150
F 0 "U2" H 9550 4350 50  0000 L CNN
F 1 "4N25" H 9750 4350 50  0000 L CNN
F 2 "Housings_DIP:DIP-6_W7.62mm" H 9550 3950 50  0001 L CIN
F 3 "" H 9750 4150 50  0001 L CNN
	1    9750 4150
	1    0    0    -1  
$EndComp
$Comp
L R R_shutter1
U 1 1 5A3AE237
P 6750 3600
F 0 "R_shutter1" V 6830 3600 50  0001 C CNN
F 1 "R_shutter" V 6750 3600 50  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 6680 3600 50  0001 C CNN
F 3 "" H 6750 3600 50  0001 C CNN
	1    6750 3600
	0    1    1    0   
$EndComp
$Comp
L R R_focus1
U 1 1 5A3AE28B
P 6750 3700
F 0 "R_focus1" V 6830 3700 50  0001 C CNN
F 1 "R_focus" V 6750 3700 50  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 6680 3700 50  0001 C CNN
F 3 "" H 6750 3700 50  0001 C CNN
	1    6750 3700
	0    1    1    0   
$EndComp
$Comp
L SW_DPST_x2 SW2
U 1 1 5A3AEA4D
P 3950 4100
F 0 "SW2" H 3950 4225 50  0000 C CNN
F 1 "SW_DPST_x2" H 3950 4000 50  0000 C CNN
F 2 "Buttons_Switches_THT:SW_DIP_x1_W7.62mm_Slide" H 3950 4100 50  0001 C CNN
F 3 "" H 3950 4100 50  0001 C CNN
	1    3950 4100
	1    0    0    -1  
$EndComp
$Comp
L Conn_01x02 J2
U 1 1 5A3AF171
P 10800 4150
F 0 "J2" H 10800 4250 50  0000 C CNN
F 1 "gpio5DRV" H 10800 3950 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x02_Pitch2.54mm" H 10800 4150 50  0001 C CNN
F 3 "" H 10800 4150 50  0001 C CNN
	1    10800 4150
	1    0    0    -1  
$EndComp
$Comp
L Conn_01x02 J3
U 1 1 5A3AF1CB
P 10800 4550
F 0 "J3" H 10800 4650 50  0000 C CNN
F 1 "gpio4DRV" H 10800 4350 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x02_Pitch2.54mm" H 10800 4550 50  0001 C CNN
F 3 "" H 10800 4550 50  0001 C CNN
	1    10800 4550
	1    0    0    -1  
$EndComp
$Comp
L Conn_01x03 J1
U 1 1 5A3AF44D
P 8950 3500
F 0 "J1" H 8950 3700 50  0000 C CNN
F 1 "TxRxGnd" H 8950 3300 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x03_Pitch2.54mm" H 8950 3500 50  0001 C CNN
F 3 "" H 8950 3500 50  0001 C CNN
	1    8950 3500
	1    0    0    -1  
$EndComp
$Comp
L Conn_01x02 PWR1
U 1 1 5A47C141
P 950 1550
F 0 "PWR1" H 950 1650 50  0000 C CNN
F 1 "Conn_01x02" H 950 1350 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x02_Pitch2.54mm" H 950 1550 50  0001 C CNN
F 3 "" H 950 1550 50  0001 C CNN
	1    950  1550
	-1   0    0    1   
$EndComp
$Comp
L LM2936-3.3 U4
U 1 1 5A4E750C
P 1650 1450
F 0 "U4" H 1500 1575 50  0000 C CNN
F 1 "LD1117S33TR_SOT223" H 1650 1575 50  0000 L CNN
F 2 "TO_SOT_Packages_SMD:TO-252-3_TabPin2" H 1650 1650 50  0001 C CNN
F 3 "" H 1750 1200 50  0001 C CNN
	1    1650 1450
	1    0    0    -1  
$EndComp
$Comp
L R R_rest_gpio16
U 1 1 5A4E7A3A
P 4100 3550
F 0 "R_rest_gpio16" V 4000 3650 50  0000 C CNN
F 1 "R" V 4100 3550 50  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 4030 3550 50  0001 C CNN
F 3 "" H 4100 3550 50  0001 C CNN
	1    4100 3550
	1    0    0    -1  
$EndComp
$Comp
L R R_gpio14
U 1 1 5A4E7D81
P 3750 3800
F 0 "R_gpio14" V 3650 3800 50  0001 C CNN
F 1 "R" V 3750 3800 50  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 3680 3800 50  0001 C CNN
F 3 "" H 3750 3800 50  0001 C CNN
	1    3750 3800
	0    1    1    0   
$EndComp
$Comp
L R R_adc1
U 1 1 5A4E7E0B
P 3750 3500
F 0 "R_adc1" V 3850 3550 50  0001 C CNN
F 1 "R" V 3750 3500 50  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 3680 3500 50  0001 C CNN
F 3 "" H 3750 3500 50  0001 C CNN
	1    3750 3500
	0    1    1    0   
$EndComp
$Comp
L R R_cs0
U 1 1 5A4E8362
P 5000 6050
F 0 "R_cs0" V 5080 6050 50  0000 C CNN
F 1 "R" V 5000 6050 50  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 4930 6050 50  0001 C CNN
F 3 "" H 5000 6050 50  0001 C CNN
	1    5000 6050
	1    0    0    -1  
$EndComp
$Comp
L R R_gpio9
U 1 1 5A4E83FB
P 5200 6050
F 0 "R_gpio9" V 5300 6050 50  0000 C CNN
F 1 "R" V 5200 6050 50  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 5130 6050 50  0001 C CNN
F 3 "" H 5200 6050 50  0001 C CNN
	1    5200 6050
	1    0    0    -1  
$EndComp
$Comp
L R R_gpio10
U 1 1 5A4E8449
P 5300 5650
F 0 "R_gpio10" V 5380 5650 50  0000 C CNN
F 1 "R" V 5300 5650 50  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 5230 5650 50  0001 C CNN
F 3 "" H 5300 5650 50  0001 C CNN
	1    5300 5650
	1    0    0    -1  
$EndComp
$Comp
L R R_mosi1
U 1 1 5A4E849E
P 5400 6050
F 0 "R_mosi1" V 5500 6050 50  0000 C CNN
F 1 "R" V 5400 6050 50  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 5330 6050 50  0001 C CNN
F 3 "" H 5400 6050 50  0001 C CNN
	1    5400 6050
	1    0    0    -1  
$EndComp
$Comp
L R R_sclk1
U 1 1 5A4E84F4
P 5500 5650
F 0 "R_sclk1" V 5580 5650 50  0000 C CNN
F 1 "R" V 5500 5650 50  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 5430 5650 50  0001 C CNN
F 3 "" H 5500 5650 50  0001 C CNN
	1    5500 5650
	1    0    0    -1  
$EndComp
$Comp
L R R_gpio0
U 1 1 5A4E873A
P 7600 3800
F 0 "R_gpio0" V 7680 3800 50  0000 C CNN
F 1 "R" V 7600 3800 50  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 7530 3800 50  0001 C CNN
F 3 "" H 7600 3800 50  0001 C CNN
	1    7600 3800
	0    1    1    0   
$EndComp
Text GLabel 2050 1450 2    60   UnSpc ~ 0
3.3V
Text GLabel 1750 2050 2    60   UnSpc ~ 0
GND
Text GLabel 2700 3900 0    60   UnSpc ~ 0
3.3V
Text GLabel 3750 4100 0    60   UnSpc ~ 0
3.3V
Text GLabel 2700 4100 0    60   UnSpc ~ 0
GND
Wire Wire Line
	10050 4150 10600 4150
Wire Wire Line
	10050 4250 10600 4250
Wire Wire Line
	10600 4550 10050 4550
Wire Wire Line
	10050 4650 10600 4650
Wire Wire Line
	6150 3500 8750 3500
Wire Wire Line
	4350 3400 4100 3400
Wire Wire Line
	4100 3700 4350 3700
Text GLabel 3000 3500 0    60   UnSpc ~ 0
GND
Text GLabel 4700 6300 0    60   UnSpc ~ 0
GND
Text GLabel 8150 3900 2    60   UnSpc ~ 0
GND
Text GLabel 6700 4100 2    60   UnSpc ~ 0
GND
Text GLabel 8150 3700 2    60   UnSpc ~ 0
3.3V
Text GLabel 9450 4250 0    60   UnSpc ~ 0
GND
Text GLabel 9450 4650 0    60   UnSpc ~ 0
GND
$Comp
L CP_Small C_uPWR1
U 1 1 5A4ED6CF
P 4350 4300
F 0 "C_uPWR1" H 4360 4370 50  0000 L CNN
F 1 "CP_Small" H 4360 4220 50  0000 L CNN
F 2 "Capacitors_Tantalum_SMD:CP_Tantalum_Case-R_EIA-2012-12_Wave" H 4350 4300 50  0001 C CNN
F 3 "" H 4350 4300 50  0001 C CNN
	1    4350 4300
	1    0    0    -1  
$EndComp
Wire Wire Line
	4150 4100 4350 4100
Wire Wire Line
	4350 4100 4350 4200
Connection ~ 4350 4100
Text GLabel 4350 4400 3    60   UnSpc ~ 0
GND
$Comp
L CP_Small C_pwrIN1
U 1 1 5A4ED969
P 1350 1800
F 0 "C_pwrIN1" H 1360 1870 50  0000 L CNN
F 1 "CP_Small" H 1360 1720 50  0000 L CNN
F 2 "Capacitors_Tantalum_SMD:CP_Tantalum_Case-R_EIA-2012-12_Wave" H 1350 1800 50  0001 C CNN
F 3 "" H 1350 1800 50  0001 C CNN
	1    1350 1800
	1    0    0    -1  
$EndComp
$Comp
L CP_Small C_pwrOUT1
U 1 1 5A4ED9CE
P 1950 1800
F 0 "C_pwrOUT1" H 1960 1870 50  0000 L CNN
F 1 "CP_Small" H 1960 1720 50  0000 L CNN
F 2 "Capacitors_Tantalum_SMD:CP_Tantalum_Case-R_EIA-2012-12_Wave" H 1950 1800 50  0001 C CNN
F 3 "" H 1950 1800 50  0001 C CNN
	1    1950 1800
	1    0    0    -1  
$EndComp
Wire Wire Line
	1350 1450 1150 1450
Wire Wire Line
	1950 1450 2050 1450
Wire Wire Line
	1350 1450 1350 1700
Wire Wire Line
	1950 1450 1950 1700
Wire Wire Line
	1150 1550 1150 1900
Wire Wire Line
	1150 1900 1950 1900
Wire Wire Line
	1650 1750 1650 2050
Connection ~ 1350 1900
Connection ~ 1650 1900
Wire Wire Line
	1650 2050 1750 2050
Connection ~ 1950 1900
Connection ~ 1350 1450
Connection ~ 1950 1450
Connection ~ 1150 1550
$Comp
L 4N25 U3
U 1 1 5A3AE1A5
P 9750 4550
F 0 "U3" H 9550 4750 50  0000 L CNN
F 1 "4N25" H 9750 4750 50  0000 L CNN
F 2 "Housings_DIP:DIP-6_W7.62mm" H 9550 4350 50  0001 L CIN
F 3 "" H 9750 4550 50  0001 L CNN
	1    9750 4550
	1    0    0    -1  
$EndComp
$Comp
L R R_gpio13
U 1 1 5A4F3FED
P 3250 4000
F 0 "R_gpio13" V 3330 4000 50  0000 C CNN
F 1 "R" V 3250 4000 50  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 3180 4000 50  0001 C CNN
F 3 "" H 3250 4000 50  0001 C CNN
	1    3250 4000
	0    1    1    0   
$EndComp
Wire Wire Line
	3400 4000 4350 4000
$Comp
L Conn_01x06 no_conn1
U 1 1 5A50EB83
P 4700 5150
F 0 "no_conn1" H 4700 5450 50  0000 C CNN
F 1 "Conn_01x06" H 4700 4750 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x06_Pitch2.54mm" H 4700 5150 50  0001 C CNN
F 3 "" H 4700 5150 50  0001 C CNN
	1    4700 5150
	-1   0    0    1   
$EndComp
Text GLabel 3450 3600 0    60   Input ~ 0
3.3V
Wire Wire Line
	3450 3600 4350 3600
$Comp
L R R_miso1
U 1 1 5A4E83AF
P 5100 5650
F 0 "R_miso1" V 5180 5650 50  0000 C CNN
F 1 "R" V 5100 5650 50  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 5030 5650 50  0001 C CNN
F 3 "" H 5100 5650 50  0001 C CNN
	1    5100 5650
	1    0    0    -1  
$EndComp
$Comp
L ESP-12E U1
U 1 1 5A3AD988
P 5250 3700
F 0 "U1" H 5250 3600 50  0000 C CNN
F 1 "ESP-12E" H 5250 3800 50  0000 C CNN
F 2 "ESP8266:ESP-12E_SMD" H 5250 3700 50  0001 C CNN
F 3 "" H 5250 3700 50  0001 C CNN
	1    5250 3700
	1    0    0    -1  
$EndComp
Wire Wire Line
	5000 4600 5000 5900
Wire Wire Line
	5100 4600 5100 5500
Wire Wire Line
	5200 4600 5200 5900
Wire Wire Line
	5300 4600 5300 5500
Wire Wire Line
	5400 4600 5400 5900
Wire Wire Line
	5500 4600 5500 5500
Wire Wire Line
	4700 6300 5500 6300
Wire Wire Line
	5500 6300 5500 5800
Wire Wire Line
	5400 6200 5400 6300
Connection ~ 5400 6300
Wire Wire Line
	5300 5800 5300 6300
Connection ~ 5300 6300
Wire Wire Line
	5200 6200 5200 6300
Connection ~ 5200 6300
Wire Wire Line
	5100 5800 5100 6300
Connection ~ 5100 6300
Wire Wire Line
	5000 6200 5000 6300
Connection ~ 5000 6300
Wire Wire Line
	4900 5350 5500 5350
Connection ~ 5500 5350
Wire Wire Line
	4900 5250 5400 5250
Connection ~ 5400 5250
Wire Wire Line
	4900 5150 5300 5150
Connection ~ 5300 5150
Wire Wire Line
	4900 5050 5200 5050
Connection ~ 5200 5050
Wire Wire Line
	4900 4950 5100 4950
Connection ~ 5100 4950
Wire Wire Line
	4900 4850 5000 4850
Connection ~ 5000 4850
Wire Wire Line
	6600 3600 6150 3600
Wire Wire Line
	6600 3700 6150 3700
Text GLabel 6900 3700 2    60   Input ~ 0
focus_sig
Text GLabel 6900 3600 2    60   Input ~ 0
shut_sig
Text GLabel 9450 4050 0    60   Input ~ 0
shut_sig
Text GLabel 9450 4450 0    60   Input ~ 0
focus_sig
Wire Wire Line
	6150 3800 7450 3800
$Comp
L R_Small R_gpio2
U 1 1 5A512848
P 6250 3900
F 0 "R_gpio2" V 6250 3850 50  0000 L CNN
F 1 "R_Small" V 6200 4050 50  0001 L CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" H 6250 3900 50  0001 C CNN
F 3 "" H 6250 3900 50  0001 C CNN
	1    6250 3900
	0    1    1    0   
$EndComp
$Comp
L R_Small R_gpio15
U 1 1 5A5128BF
P 6250 4000
F 0 "R_gpio15" V 6250 3900 50  0000 L CNN
F 1 "r_gpio15" V 6300 3950 50  0001 L CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" H 6250 4000 50  0001 C CNN
F 3 "" H 6250 4000 50  0001 C CNN
	1    6250 4000
	0    1    1    0   
$EndComp
Wire Wire Line
	6150 4100 6700 4100
Wire Wire Line
	6350 4000 6550 4000
Wire Wire Line
	6550 3900 6550 4100
Connection ~ 6550 4100
Wire Wire Line
	6350 3900 6550 3900
Connection ~ 6550 4000
Wire Wire Line
	6150 3400 8750 3400
Text GLabel 8750 3600 0    60   Input ~ 0
GND
Wire Wire Line
	3900 3800 4350 3800
Wire Wire Line
	3900 3500 4350 3500
Wire Wire Line
	3000 3500 3600 3500
Wire Wire Line
	3100 3500 3100 3800
Wire Wire Line
	3100 3800 3600 3800
Connection ~ 3100 3500
Wire Wire Line
	4350 3900 3900 3900
Wire Wire Line
	3900 3900 3900 3800
Connection ~ 3900 3800
$Comp
L SW_SPDT Prompt_switch1
U 1 1 5A5563EC
P 2900 4000
F 0 "Prompt_switch1" H 2900 4170 50  0000 C CNN
F 1 "SW_SPDT" H 2900 3800 50  0000 C CNN
F 2 "Buttons_Switches_SMD:SW_SPST_B3S-1100" H 2900 4000 50  0001 C CNN
F 3 "" H 2900 4000 50  0001 C CNN
	1    2900 4000
	-1   0    0    1   
$EndComp
$Comp
L SW_SPDT Bootloader_W1
U 1 1 5A55646E
P 7950 3800
F 0 "Bootloader_W1" H 7950 3970 50  0000 C CNN
F 1 "SW_SPDT" H 7950 3600 50  0000 C CNN
F 2 "Buttons_Switches_SMD:SW_SPST_B3S-1100" H 7950 3800 50  0001 C CNN
F 3 "" H 7950 3800 50  0001 C CNN
	1    7950 3800
	1    0    0    -1  
$EndComp
$EndSCHEMATC
